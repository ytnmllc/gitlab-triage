# frozen_string_literal: true

require 'optparse'
require 'yaml'
require 'gitlab/triage/engine'
require 'gitlab/triage/options'
require 'gitlab/triage/option_parser'

RSpec.shared_context 'with integration context' do
  let(:argv) { %W[--source-id #{project_id} --token #{token}] }
  let(:options) { Gitlab::Triage::OptionParser.parse(argv) }
  let(:token) { 'token' }

  include_context 'with stubs context'

  def perform(yaml)
    policies = HashWithIndifferentAccess.new(YAML.safe_load(yaml))
    options.dry_run = false

    expect do
      Gitlab::Triage::Engine
        .new(policies: policies, options: options)
        .perform
    end.to output.to_stdout
  end
end
